/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.human;

/**
 *
 * @author lenovo
 */
public class Male extends Human { //Inheritance จาก class Human

    public Male(String name, String gender, int age) {
        super(name, gender, age);
    }

    @Override //Overide และเปลี่ยนการทำงานใน method 
    public void talk() {
        System.out.println("What up!!");
    }
}
