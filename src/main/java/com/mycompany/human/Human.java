/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.human;

/**
 *
 * @author lenovo
 */
public class Human {

//attribute
    protected String firstname; // protected คือการป้องกันตัวแปรให้สามารถนำไปใช้ได้ เฉพาะใน class ของตัวเอง และ supclass เท่านั้น
    protected String lastname;
    protected String name; //สร้างตัวแปร name ไว้เก็บ "ชื่อ"
    protected String gender; //สร้างตัวแปร gender ไว้เก็บ "เพศ"
    protected int age; //สร้างตัวแปร age ไว้เก็บ "อายุ"

//constructor
    public Human(String name, String gender, int age) {
        this.name = name;
        this.gender = gender;
        this.age = age;
    }

//method
    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public int getAge() {
        return age;
    }

    public void introduce() {
        System.out.println("::Introduce::");
        System.out.println("Name : " + this.name);
        System.out.println("Gender : " + this.gender);
        System.out.println("Age : " + this.age);
    }

    public void walk() {
        System.out.println(this.name + " is walking");
    }

    public void talk() {
        System.out.println(this.name+" Speak :: What up mann!!!!");
        System.out.println("------------------------------------------");
    }
    //การทำ Overlode 
    public String getName(String firstname, String lastname){
        return firstname + lastname;
    }

}

