package com.mycompany.human;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author lenovo
 */
public class TestHuman {
    public static void main(String[] args) {
        Human jack = new Human("Jack", "Male", 31); //สร้าง object jack
        jack.introduce(); //เรียกใช้ method introduce
        jack.walk(); //เรียกใช้ method walk
        jack.talk(); // เรียกใช้ method talk
        Male mac = new Male("Mac", "Male", 33); //สร้าง object mac
        mac.introduce();
        mac.walk();
        mac.talk();
        Male juk = new Male("Juk", "Male", 29); //สร้าง object juk
        juk.introduce();
        juk.walk();
        juk.talk();
    }
}
